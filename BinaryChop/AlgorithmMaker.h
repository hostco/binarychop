//
//  AlgorithmMaker.h
//  BinaryChop
//
//  Created by Howard Cohen on 10/7/16.
//  Copyright © 2016 Timefold.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface AlgorithmMaker : NSObject

-(AlgorithmCompetionHandler) makeAlgorithm1;
-(AlgorithmCompetionHandler) makeAlgorithm2;
-(AlgorithmCompetionHandler) makeAlgorithm3;
-(AlgorithmCompetionHandler) makeAlgorithm4;
-(AlgorithmCompetionHandler) makeAlgorithm5;
-(AlgorithmCompetionHandler) makeAlgorithm:(NSString *)algkey;

@end
