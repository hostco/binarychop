//
//  AlgorithmMaker.m
//  BinaryChop
//
//  Created by Howard Cohen on 10/7/16.
//  Copyright © 2016 Timefold.com. All rights reserved.
//

#import "AlgorithmMaker.h"
#import "Constants.h"

@implementation AlgorithmMaker

-(AlgorithmCompetionHandler) makeAlgorithm:(NSString *)algkey {
    if ( [algkey isEqualToString:(NSString *)kKeyAlgorithm1] ) {
        return [self makeAlgorithm1];
    } else if ( [algkey isEqualToString:(NSString *)kKeyAlgorithm2] ) {
            return [self makeAlgorithm2];
    } else if ( [algkey isEqualToString:(NSString *)kKeyAlgorithm3] ) {
        return [self makeAlgorithm3];
    } else if ( [algkey isEqualToString:(NSString *)kKeyAlgorithm4] ) {
        return [self makeAlgorithm4];
    } else if ( [algkey isEqualToString:(NSString *)kKeyAlgorithm5] ) {
        return [self makeAlgorithm5];
    }
    
    return nil;
}

/*
 Algorithm 1 is the first one that occurred to me.  It iterates by moving left or right by an amount that reduces based on dividing the entire length of the array by increasing powers of 2.  Those powers are 1 + the number of iterations through the array.
 
 One subtlety I learned in implementing this algorithm was that when the amount to be moved reduces to 1, we cannot go back the way we came or we will oscillate.  This oscillation happens when the list contains values that are non-contiguous (e.g. a hole in the list of values) and the number we are looking for is within that hole.
 */

-(AlgorithmCompetionHandler) makeAlgorithm1 {
    AlgorithmCompetionHandler alg1=^int (int findme, NSArray *arr, int expect) {
        
        if ( arr.count == 0 ) {
            return -1;
        } else if ( arr.count == 1 && [arr[0] intValue] != findme ) {
            return -1;
        } else {
            // 2 or more elements
            int lookat=(int) arr.count / 2;  // integer math truncates, so 3 / 2 will = 1
            int depth=1;
            BOOL found=NO;
            BOOL unit_forward=NO;
            BOOL unit_backward=NO;
            
            while( !found ) {
                // are we currently looking at the right number?  If so, this is the ending case
                if ( lookat >= arr.count ) {
                    lookat=(int) arr.count - 1;
                } else if ( lookat < 0 ) {
                    lookat=0;
                }
                
                int valAtPos=[arr[lookat] intValue];
                if ( valAtPos == findme ) {
                    found=YES;
                    break;
                }
                
                // no, we aren't looking at the right number at this position, so we either jump to the left or the right and continue searching
                
                
                
                int jump=(int) (arr.count + 1) / (int) pow(2.0, (double) depth + 1);
                
                // how do we prevent oscillation when looking for a missing 3 between 2 and 4 in two adjacent slots?
                // One way is to detect the oscillation
                // One way is for the math to determine it is time to stop looking
                /*
                 
                 any solution will require that we know we are in the endgame - it will always be that jump is 1 in the endgame where oscillation can occur.
                 
                 If jump is 1 we get one try in any one direction and then we are done.  It never makes sense to move backwards by the same number we just moved forwards, because we'd be testing the same element.  When jump falls to 1, we never want to move back. Which is to say, we can only have jump == 1 once.  Once it has reached 1, a second jump of 1 in the opposite direction can never be allowed.  Could rounding mean two jumps in the same direction?  Possibly, but in any case, backward is the oscillation we are trying to kill.
                 
                 */
                
                if ( valAtPos > findme ) {
                    // we want the "left" part  
                    
                    if ( jump < 1 ) {
                        if ( unit_forward ) {
                            // we're moving backward by 1 when we have already moved forward by 1
                            break;
                        } else {
                            jump=1;
                            unit_backward=YES;
                        }
                    }
                    
                    lookat-=jump;
                    if ( lookat < 0 ) {
                        // we bumped off the left (low) side of the array
                        break;
                    }
                    
                } else {
                    // we want the "right" part
                    
                    if ( jump < 1 ) {
                        if ( unit_backward ) {
                            // we're moving forward by 1 when we have already moved backward by 1
                            break;
                        } else {
                            jump=1;
                            unit_forward=YES;
                        }
                    }
                    
                    
                    lookat+=jump;
                    
                    if ( lookat >= arr.count ) {
                        // we bumped off the right (high) side of the array
                        break;
                    }
                }
                
                depth++;
            }
            
            if ( found ) {
                return lookat;
            }
        }
        
        return -1;
    };
    
    return alg1;
}  


/*
 Algorithm 2 will start with a value that is half the size of the array and in each iteration it will divide that by half.
 
 I started with the original algorithm and replaced the complex expression involving pow() with a simpler division of the chunksize by 2 each time.  That meant there was no need for depth.  I eliminated some needless conditions at the top.
 
 The akward forward/backward detection during the endgame reamains.
 */

-(AlgorithmCompetionHandler) makeAlgorithm2 {
    AlgorithmCompetionHandler alg2=^int (int findme, NSArray *arr, int expect) {
        
        if ( arr.count == 0 ) {
            return -1;
        } else if ( arr.count == 1 && [arr[0] intValue] != findme ) {
            return -1;
        } else {
            // 2 or more elements
            
            int chunksize=(int) arr.count / 2;  // integer math truncates, so 3 / 2 will = 1
            int lookat=chunksize;  
            BOOL found=NO;
            BOOL unit_forward=NO;
            BOOL unit_backward=NO;
            
            while( !found ) {
                // are we currently looking at the right number?  If so, this is the ending case                
                int valAtPos=[arr[lookat] intValue];
                if ( valAtPos == findme ) {
                    found=YES;
                    break;
                }
                
                // no, we aren't looking at the right number at this position, so we either jump to the left or the right and continue searching
                
                
                chunksize/=2;
                
                if ( valAtPos > findme ) {
                    // we want the "left" part  
                    
                    if ( chunksize < 1 ) {
                        if ( unit_forward ) {
                            // we're moving backward by 1 when we have already moved forward by 1
                            break;
                        } else {
                            chunksize=1;
                            unit_backward=YES;
                        }
                    }
                    
                    lookat-=chunksize;
                    if ( lookat < 0 ) {
                        // we bumped off the left (low) side of the array
                        break;
                    }
                    
                } else {
                    // we want the "right" part
                    if ( chunksize < 1 ) {
                        if ( unit_backward ) {
                            // we're moving forward by 1 when we have already moved backward by 1
                            break;
                        } else {
                            chunksize=1;
                            unit_forward=YES;
                        }
                    }
                    
                    
                    lookat+=chunksize;
                    
                    if ( lookat >= arr.count ) {
                        // we bumped off the right (high) side of the array
                        break;
                    }
                }
            }
            
            if ( found ) {
                return lookat;
            }
        }
        
        return -1;
    };
    
    return alg2;
}


/*
 Algorithm 3 will take the approach of dividing the array. This won't be as efficient, but it might not be tragic.
 
 What was subtle about this one was that I had to start keeping track of the position within the original array when in fact the array is getting shorter and shorter.
 
 The forward/backward oscillation never appeared in this algorithm, and I found a bug in the initial logic that allows an array with a single element through.  Apparently the previous 2 algorithms worked fine with one element and I didn't know it.  But, algorithm3 didn't work with one element in it, at least while it was being developed.
 
 */
-(AlgorithmCompetionHandler) makeAlgorithm3 {
    
    AlgorithmCompetionHandler alg3=^int (int findme, NSArray *arr, int expect) {
        
        if ( arr.count == 0 ) {
            return -1;
        } else if ( arr.count == 1 ) {
            if ( [arr[0] intValue] == findme ) {
                return 0;
            } else {
                return -1;
            }
        } else {
            // 2 or more elements
            
            BOOL done=NO;
            BOOL found=NO;
            NSArray *marray=arr;
            int midpoint=-1;
            int relpos=(int) marray.count/2;
            
            while(!done) {
                midpoint=(int) marray.count/2;
                
                int valAtPos=[marray[midpoint] intValue];
                if ( valAtPos == findme ) {
                    found=YES;
                    break;
                }
                
                NSRange range;
                if ( valAtPos > findme ) {
                    if ( midpoint < 1) {
                        break;
                    } else {
                        range=NSMakeRange(0, midpoint);
                        relpos-=(midpoint+1)/2;
                    }
                    
                } else {
                    if ( midpoint >= marray.count - 1 ) {
                        break;
                    } else {
                        int remain=(int) marray.count - midpoint;
                        
                        range=NSMakeRange(midpoint, remain);    
                        relpos+=(remain)/2;
                    }
                }
                
                marray=[marray subarrayWithRange:range];
                
                if ( marray.count == 0 ) {
                    break;
                }
            }
            
            if ( found ) {
                return relpos;
            } else {
                return -1;
            }
        }
    };
    
    return alg3;
}


/*
 
 Algorithm 4 will be recursive and in each iteration it will accept a sub-range of the array and check it's center.
 
 What I learned from this one, aside from recursive code being more subtle to create and debug, is that needed to keep in sync its idea of relpos with the actual position in the sub array.  This was a similar problem with a similar solution.  Relpos was passed through and maintained by each recursive layer of the algorithm.
 
 */
-(AlgorithmCompetionHandler) makeAlgorithm4 {
    __weak AlgorithmMaker *weakSelf=self;
    AlgorithmCompetionHandler alg4=^int (int findme, NSArray *arr, int expect) {
        if ( arr.count == 0 ) {
            return -1;
        }
        
        int result=[weakSelf recursivelyFind:findme relpos:(int) arr.count/2 array:arr];
        return result;
    };
    
    return alg4;
}

-(int) recursivelyFind:(int)findme relpos:(int)relpos array:(NSArray *)arr {
    int midpoint=(int) arr.count/2;
    
    int valAtPos=[arr[midpoint] intValue];
    if ( valAtPos == findme ) {
        return relpos;
    }
    
    NSRange range;
    if ( valAtPos > findme ) {
        if ( midpoint < 1) {
            return -1;
        } else {
            range=NSMakeRange(0, midpoint);
            relpos-=(midpoint+1)/2;
        }
        
    } else {
        if ( midpoint >= arr.count - 1 ) {
            return -1;
        } else {
            int remain=(int) arr.count - midpoint;
            
            range=NSMakeRange(midpoint, remain);    
            relpos+=remain/2;
        }
    }
    
    arr=[arr subarrayWithRange:range];
    
    if ( arr.count == 0 ) {
        return -1;
    }
    
    int result=[self recursivelyFind:findme relpos:relpos array:arr];
    
    return result;    
}

/*
 Algorithm5 will work from the idea that a read head will move along the array at some position and its current and last position will be known.  If it doesn't land on the result it will calculate its next jump based on half the difference between its current and last position, either plus or minus depending on whether it is higher or lower. 
 */

-(AlgorithmCompetionHandler) makeAlgorithm5 {
    AlgorithmCompetionHandler alg5=^int (int findme, NSArray *arr, int expect) {
        
        if ( arr.count == 0 ) {
            return -1;
        }
        
        int readat=(int) arr.count/2;
        int lastReadat=0;
        BOOL unit_forward=NO;
        BOOL unit_backward=NO;
        
        while( readat >= 0 ) {
            int valAtPos=[arr[readat] intValue];
            if ( valAtPos == findme ) {
                return readat;
            }
            
            int offset=abs(readat - lastReadat)/2;
            
            if ( valAtPos > findme ) {
                if ( offset == 0 ) {
                    if ( unit_forward ) {
                        // we're moving backward by 1 when we have already moved forward by 1
                        readat=-1;
                    } else {
                        offset=1;
                        unit_backward=YES;
                        readat-=offset;
                    }
                }
                
            } else {
                if ( unit_backward ) {
                    // we're moving forward by 1 when we have already moved backward by 1
                    readat=-1;
                } else {
                    offset=1;
                    unit_forward=YES;
                    readat+=offset;
                }
            }
            
            if ( readat >= arr.count ) {
                readat=-1;
            }
            
            lastReadat=readat;
        }
        
        return readat;
    };
    
    return alg5;
}




@end
