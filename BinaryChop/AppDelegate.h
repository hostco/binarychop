//
//  AppDelegate.h
//  BinaryChop
//
//  Created by Howard Cohen on 10/5/16.
//  Copyright © 2016 Timefold.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

