//
//  ChopOperation.h
//  BinaryChop
//
//  Created by Howard Cohen on 10/7/16.
//  Copyright © 2016 Timefold.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ConcurrencyManager;

@interface ChopOperation : NSOperation {
    int findme;
    int expect;
    int seq;
    NSArray *array;
    NSString *algkey;
    ConcurrencyManager * __weak ccm;

}

@property (nonatomic) int findme, expect, seq;
@property (nonatomic, strong) NSArray *array;
@property (nonatomic, strong) NSString *algkey;
@property (nonatomic, weak) ConcurrencyManager *ccm;


- (ChopOperation *) initToFind:(int)findme array:(NSArray *)array expect:(int)expect algkey:(NSString *) algkey_p forCCM:(ConcurrencyManager *) ccm_p seq:(int) seq_p;  

@end
