//
//  ChopOperation.m
//  BinaryChop
//
//  Created by Howard Cohen on 10/7/16.
//  Copyright © 2016 Timefold.com. All rights reserved.
//

#import "ChopOperation.h"
#import "ConcurrencyManager.h"
#import "AlgorithmMaker.h"
#import "ViewController.h"
#import "QuatluuRequestOperation.h"
#import "Constants.h"

@interface ChopOperation () {
}


@end

@implementation ChopOperation
@synthesize findme, expect, array, algkey, ccm, seq;

- (ChopOperation *) initToFind:(int)findme_p array:(NSArray *)array_p expect:(int)expect_p algkey:(NSString *) algkey_p forCCM:(ConcurrencyManager *) ccm_p seq:(int) seq_p {
    
    if ( self = [super init] ) {
        self.findme=findme_p;
        self.expect=expect_p;
        self.array=array_p;
        self.algkey=algkey_p;
        self.ccm=ccm_p;
        self.seq=seq_p;
    }
    
    return self;
}

- (void)main {
    if (![self isCancelled]) {
        [self.ccm runAlgorithm:self];
    }
}

@end
