//
//  ChopWorker.h
//  BinaryChop
//
//  Created by Howard Cohen on 10/8/16.
//  Copyright © 2016 Timefold.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@class ConcurrencyManager;

@interface ChopWorker : NSOperation {
    NSString *algkey;
    ConcurrencyManager * __weak ccm;
}

@property (nonatomic, strong) NSString *algkey;
@property (nonatomic, weak) ConcurrencyManager *ccm;

- (ChopWorker *) initForAlgkey:(NSString *) algkey_p forCCM:(ConcurrencyManager *) ccm_p;

@end
