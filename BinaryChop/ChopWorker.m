//
//  ChopWorker.m
//  BinaryChop
//
//  Created by Howard Cohen on 10/8/16.
//  Copyright © 2016 Timefold.com. All rights reserved.
//

#import "ChopWorker.h"
#import "ChopOperation.h"
#import "QuatluuRequestOperation.h"
#import "Constants.h"
#import "ConcurrencyManager.h"
#import "ViewController.h"

@implementation ChopWorker
@synthesize algkey, ccm;

- (ChopWorker *) initForAlgkey:(NSString *) algkey_p forCCM:(ConcurrencyManager *) ccm_p {
    if ( self = [super init] ) {
        self.algkey=algkey_p;
        self.ccm=ccm_p;
    }
    
    return self;
}
- (void)main {
    if (![self isCancelled]) {
        QuatluuRequestOperation *qro=[[QuatluuRequestOperation alloc] initForAlgorithm:self.algkey forCCM:(ConcurrencyManager *) self.ccm ];
        
        if ( qro != nil ) {
            BOOL debug=CHOP_OPERATION_DEBUG;
            if ( debug ) NSLog(@"%s: requesting qualuu for algorithm %@", __PRETTY_FUNCTION__, self.algkey);
            [self.ccm.quatluuRequestQueue addOperation:qro];            
        }        
    }
}



@end
