//
//  ConcurrencyManager.h
//  BinaryChop
//
//  Created by Howard Cohen on 10/7/16.
//  Copyright © 2016 Timefold.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AlgorithmMaker;
@class ViewController;
@class ChopOperation;

@interface ConcurrencyManager : NSObject {
    NSOperationQueue *quatluuRequestQueue;  // single-threaded access to quatluus
    
    NSMutableDictionary *workerQueues; 
    int quatluus;
    AlgorithmMaker *amaker;
    NSMutableDictionary *algorithmTestsPassed, *algorithmTestsAttempted;
    
    ViewController * __weak viewController;
    
    NSArray *allTests;
    NSMutableArray *buildAllTests;
    NSArray *longArray, *veryLongArray;


}

@property (nonatomic, strong) NSOperationQueue * quatluuRequestQueue;

@property (nonatomic, strong) NSMutableDictionary *workerQueues;
@property (nonatomic, strong) AlgorithmMaker *amaker;
@property (nonatomic, weak) ViewController *viewController;
@property (nonatomic) int quatluus;
@property (atomic, strong) NSMutableDictionary *algorithmTestsPassed, *algorithmTestsAttempted;

@property (nonatomic, strong)     NSArray *longArray, *veryLongArray;
@property (nonatomic, strong)     NSMutableArray *buildAllTests;
@property (nonatomic, strong)     NSArray *allTests;



+(ConcurrencyManager *)getConcurrencyManager;

// -(BOOL) getquatluuToFind:(int)findme array:(NSArray *)array expect:(int)expect algkey:(NSString *) algkey;

-(void) addTestcaseToFind:(int)findme array:(NSArray *) array expect:(int) expect;

-(void) bldAllTests;
-(void) resetAllTests;
-(void) runAlgorithm:(ChopOperation *) co;
-(BOOL) queueNextActionFor:(NSString *)algkey;

@end
