//
//  ConcurrencyManager.m
//  BinaryChop
//
//  Created by Howard Cohen on 10/7/16.
//  Copyright © 2016 Timefold.com. All rights reserved.
//

#import "ConcurrencyManager.h"
#import "Constants.h"
#import "ChopOperation.h"
#import "AlgorithmMaker.h"
#import "QuatluuRequestOperation.h"
#import "ViewController.h"
#import "ChopWorker.h"

@interface ConcurrencyManager () {
    BOOL debug;
}

@property (nonatomic) BOOL debug;

@end

@implementation ConcurrencyManager 
@synthesize quatluuRequestQueue, quatluus, amaker, algorithmTestsPassed, algorithmTestsAttempted, viewController, workerQueues, allTests, longArray, veryLongArray, buildAllTests, debug;

+(ConcurrencyManager *)getConcurrencyManager {
    static ConcurrencyManager *sharedManager=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager=[[ConcurrencyManager alloc] init];
    });
                  
    return(sharedManager);
}

-(ConcurrencyManager *) init {
    if ( self = [super init] ) {
        
        self.debug=CCMGR_DEBUG;
        if ( self.debug ) NSLog(@"%s: setting up long arrays", __PRETTY_FUNCTION__);

        // make some longer arrays
        NSMutableArray *tmpArray=[NSMutableArray new];
        for(int i = 1; i <= LONG_ARRAY_SIZE; i++) {
            [tmpArray addObject:[NSNumber numberWithInt:i]];
        }
        
        self.longArray=[NSArray arrayWithArray:tmpArray];
        
        [tmpArray removeAllObjects];
        
        for(int i = 1; i <= 4000; i++) {
            [tmpArray addObject:[NSNumber numberWithInt:i]];
        }
        
        self.veryLongArray=[NSArray arrayWithArray:tmpArray];

        
        if ( self.debug ) NSLog(@"%s: setting up quatluu request queue", __PRETTY_FUNCTION__);

        self.quatluuRequestQueue=[[NSOperationQueue alloc] init];
        if ( self.quatluuRequestQueue == nil ) {
            NSLog(@"ERROR: cannot allocate quatluu operation queue");
        } else {
            self.quatluuRequestQueue.name=@"Quatluu Request Queue";
            self.quatluuRequestQueue.maxConcurrentOperationCount=1;

        }
        
        if ( self.debug ) NSLog(@"%s: setting up algorithm and worker queues", __PRETTY_FUNCTION__);

        self.workerQueues=[NSMutableDictionary new];
        
        for(int q=1; q<=5; q++) {
            NSString *algkey=[NSString stringWithFormat:@"%d", q];
            
            NSOperationQueue *wqueue=[[NSOperationQueue alloc] init];
            if ( wqueue == nil ) {
                NSLog(@"ERROR: cannot allocate algorithm                  %d worker queue", q);
            } else {
                self.workerQueues[algkey]=wqueue;
                wqueue.name=[NSString stringWithFormat:@"Algorithm %@ Worker Queue", algkey];
                wqueue.maxConcurrentOperationCount=1;
            }
        }   
        
        self.quatluus=TOTAL_AVAILABLE_QUATLUUS;
        
        if ( self.debug ) NSLog(@"%s: FYI: initial quatluus=%d", __PRETTY_FUNCTION__, (int) self.quatluus);

        self.amaker=[[AlgorithmMaker alloc] init];
        
        [self bldAllTests];
    }
    
    return self;
}

// pass a 1-justified test sequence number 
-(BOOL) queueNextActionFor:(NSString *)algkey {
    NSNumber *lastTestRun=nil;
    int nextTest=1;
    BOOL result=NO;

    @synchronized (self.algorithmTestsAttempted) {
        lastTestRun=self.algorithmTestsAttempted[algkey];
                if ( lastTestRun == nil ) {
            lastTestRun=@0;
        }
        
        
        nextTest=[lastTestRun intValue]+1;
        

        self.algorithmTestsAttempted[algkey]=[NSNumber numberWithInt:nextTest];
    }
    
    if ( self.debug ) NSLog(@"%s: nextTest=%d, self.allTests.count=%d for algorithm %@", __PRETTY_FUNCTION__, nextTest, (int) self.allTests.count, algkey);
    
    nextTest--; // array is zero-justified
    
    if ( nextTest < 0 ) {
        if ( self.debug ) NSLog(@"%s: encountered negative test case for algorithm %@", __PRETTY_FUNCTION__, algkey);

    } else if ( nextTest >= (int) self.allTests.count ) {
        if ( self.debug ) NSLog(@"%s: no more test cases for algorithm %@", __PRETTY_FUNCTION__, algkey);
    } else {
        // NSOperationQueue *queue=self.algorithmQueues[algkey];
        NSOperationQueue *queue=self.workerQueues[algkey];
        if ( queue == nil ) {
            NSLog(@"%s: ERROR: cannot retrieve queue for algorithm %@", __PRETTY_FUNCTION__, algkey);
        } else {
            ChopOperation *co=self.allTests[nextTest];
            
            if ( self.debug ) NSLog(@"%s: running test case %d for algorithm %@", __PRETTY_FUNCTION__, nextTest, algkey);

            ChopOperation *specific_co=[[ChopOperation alloc] initToFind:co.findme array:co.array expect:co.expect algkey:algkey forCCM:self seq:nextTest+1];
            
            if ( specific_co == nil ) {
                NSLog(@"%s: ERROR: cannot create ChopOperation for algorithm %@", __PRETTY_FUNCTION__, algkey);
            } else {
                [queue addOperation:specific_co];
            }
        }
        
        result=YES;
    }
    
    return(result);
}



-(void) addTestcaseToFind:(int)findme array:(NSArray *) array expect:(int) expect {
    
    ChopOperation *co=[[ChopOperation alloc] initToFind:findme array:array expect:expect algkey:(NSString *)kKeyAlgorithmNone forCCM:self seq:(int) self.buildAllTests.count + 1];
    
    if ( co == nil ) {
        NSLog(@"ERROR: cannot create generic ChopOperation to find %d expecting %d in list with %d elements", findme, expect, (int) array.count);
    } else {
        [self.buildAllTests addObject:co];
    }
}

-(void) runAlgorithm:(ChopOperation *) co {
    AlgorithmCompetionHandler algorithmBlock=[self.amaker makeAlgorithm:co.algkey];
    
    //NSLog(@"%s: TESTING: (1) algorithm %@ looking for %d in %d elements, expecting %d", __PRETTY_FUNCTION__, co.algkey, co.findme, (int) co.array.count, co.expect);

    int result=algorithmBlock(co.findme, co.array, co.expect);
    for(int redund=2; redund <= REDUNDANT_CALCULATION_MULTIPLIER; redund++) {
        // NSLog(@"%s: TESTING: (%d) algorithm %@ looking for %d in %d elements, expecting %d", __PRETTY_FUNCTION__, redund, co.algkey, co.findme, (int) co.array.count, co.expect);
        algorithmBlock(co.findme, co.array, co.expect);
    }
    
    int currentPassed=0;
    @synchronized (self.algorithmTestsPassed) {
        NSNumber *currentNumber=self.algorithmTestsPassed[co.algkey];
        currentPassed=[currentNumber intValue];
        
        if ( result == co.expect) {
            currentPassed++;  
            self.algorithmTestsPassed[co.algkey]=[NSNumber numberWithInt:currentPassed];
            
            if ( self.debug ) { 
                NSLog(@"%s: TEST PASSED: for algorithm %@", __PRETTY_FUNCTION__, co.algkey);
            }
            
        } else {
            // test failed
            
            NSLog(@"TEST FAILED (alg=%@): expect %d but received %d when trying to find %d in array with %d elements", co.algkey, co.expect, result, co.findme, (int) co.array.count);
        }
    }
    
    int currentAttempted=0;
    @synchronized (self.algorithmTestsAttempted) {
        NSNumber *attemptedNumber=self.algorithmTestsAttempted[co.algkey];
        currentAttempted=[attemptedNumber intValue];
    }
    
    NSString *algoResults=[NSString stringWithFormat:@"%d of %d passed", currentPassed, currentAttempted];
    
    // make sure the UI is updated on the main thread in case this isn't running on the main thread already
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.viewController updateUIForAlgkey:co.algkey results:algoResults];
    });
    
    // if we were able to get a quatluu we are still in the game and can run our next iteration.  If not, we never put anything else on the worker queue and that algorithm stops running (is out of the game).
    
    NSOperationQueue *queue=self.workerQueues[co.algkey];
    if ( queue == nil ) {
        NSLog(@"ERROR: could not fetch worker queue for algorithm %@", co.algkey);
    } else {
        if ( self.debug ) NSLog(@"%s: FYI: pushing a ChopWorker operation onto queue for algorithm %@", __PRETTY_FUNCTION__, co.algkey);
        
        ChopWorker *worker=[[ChopWorker alloc] initForAlgkey:co.algkey forCCM:self];
        [queue addOperation:worker];
    }
}

-(void) resetAllTests {
    self.algorithmTestsPassed=[NSMutableDictionary new];
    self.algorithmTestsAttempted=[NSMutableDictionary new];
    self.quatluus=TOTAL_AVAILABLE_QUATLUUS;
}

-(void) bldAllTests {
    BOOL runPasses=YES;
    BOOL runFails=YES;
    int minElements=0;
    
    if ( YES || CCMGR_DEBUG ) NSLog(@"%s: building all test cases", __PRETTY_FUNCTION__);

    __weak ConcurrencyManager *weakself=self;
    
    self.buildAllTests=[NSMutableArray new];
    
    if ( !runPasses ) {
        NSLog(@"WARNING: passing tests have been disabled");
    }
    
    if ( !runFails ) {
        NSLog(@"WARNING: failing tests have been disabled");
    }
    
    if ( minElements != 0 ) {
        NSLog(@"WARNING: running tests on arrays with %d elements or more", minElements);
    }
    
    if ( minElements < 1 ) {
        
        NSArray *one_element=@[@1];
        
        // no elements in array
        if ( runFails ) {
            [weakself addTestcaseToFind:3 array:[NSArray new] expect:-1];
        }
        
        // one element in array
        if ( runFails ) {
            [weakself addTestcaseToFind:3 array:one_element expect:-1];
            
            [weakself addTestcaseToFind:0 array:one_element expect:-1];
        }
        if ( runPasses ) {
            [weakself addTestcaseToFind:1 array:one_element expect:0];
        }
    }
    
    if ( minElements < 2 ) {
        NSArray *two_elements=@[@1, @2];
        
        // two elements in array
        if ( runFails ) {
            [weakself addTestcaseToFind:0 array:two_elements expect:-1];
            [weakself addTestcaseToFind:2 array:two_elements expect:1];
            [weakself addTestcaseToFind:3 array:two_elements expect:-1];
        }
        
        if ( runPasses ) {
            [weakself addTestcaseToFind:1 array:two_elements expect:0];
        }
    }
    
    if ( minElements < 3 ) {
        // three elements in array
        NSArray *three_elements=@[@1, @3, @5];
        
        if ( runPasses ) {
            [weakself addTestcaseToFind:1 array:three_elements expect:0];
            [weakself addTestcaseToFind:1 array:three_elements expect:0];
            [weakself addTestcaseToFind:3 array:three_elements expect:1];
            [weakself addTestcaseToFind:5 array:three_elements expect:2];
        }
        
        
        if ( runFails ) {
            [weakself addTestcaseToFind:0 array:three_elements expect:-1];
            [weakself addTestcaseToFind:2 array:three_elements expect:-1];
            [weakself addTestcaseToFind:4 array:three_elements expect:-1];
            [weakself addTestcaseToFind:6 array:three_elements expect:-1];
        }
    }
    
    
    if ( minElements < 4 ) {
        NSArray *four_elements=@[@1, @3, @5, @7];
        
        if ( runPasses ) {
            // four elements in array, success cases
            [weakself addTestcaseToFind:1 array:four_elements expect:0];
            [weakself addTestcaseToFind:3 array:four_elements expect:1];
            [weakself addTestcaseToFind:5 array:four_elements expect:2];  
            [weakself addTestcaseToFind:7 array:four_elements expect:3];
        }
        
        if ( runFails ) {
            // four elements in array, failure cases
            [weakself addTestcaseToFind:0 array:four_elements expect:-1];
            [weakself addTestcaseToFind:2 array:four_elements expect:-1];
            [weakself addTestcaseToFind:4 array:four_elements expect:-1];
            [weakself addTestcaseToFind:6 array:four_elements expect:-1];
            [weakself addTestcaseToFind:8 array:four_elements expect:-1];
        }
    }
    
    // we want to explore the behavior around lists near 128 elements.  We'll skip the first 124 elements and then we'll create a bunch of arrays of length 125 through 155 and look for some test cases in each of those arrays.  The idea is to try to bump up against any edge cases related to rounding.
    
    NSMutableArray *growingArray=[NSMutableArray new];
    for(int i=1; i < 125; i++) { 
        [growingArray addObject:[NSNumber numberWithInt:i*2]];
    }
    
    // now add test cases for arrays with 125 through 155 members
    for(int i=125; i <= 155; i++) {
        [growingArray addObject:[NSNumber numberWithInt:i*2]];
        
        NSArray *readonlyArray=[NSArray arrayWithArray:growingArray];
        
        if ( runPasses ) {
            [weakself addTestcaseToFind:i*2 array:[NSArray arrayWithArray:readonlyArray] expect:i-1];
            [weakself addTestcaseToFind:2 array:readonlyArray expect:0];
        }
        
        if ( runFails ) {
            [weakself addTestcaseToFind:i*2-1 array:readonlyArray expect:-1];
            [weakself addTestcaseToFind:i*2+1 array:readonlyArray expect:-1];
            [weakself addTestcaseToFind:1 array:readonlyArray expect:-1];
            [weakself addTestcaseToFind:3 array:readonlyArray expect:-1];
        }
    }
    
    if ( minElements < LONG_ARRAY_SIZE ) {
        // a hundred elements in array
        
        NSArray *readonlyLongArray=[NSArray arrayWithArray:self.longArray];
        
        for(int i=0; i <= LONG_ARRAY_SIZE; i+=1) { 
            // the arrays start at 1, so 0 (our first number in this loop) isn't in the array and should yield -1 as the result because all non-found numbers yield -1
            if ( runPasses || (runFails && i == 0) ) {
                [weakself addTestcaseToFind:i array:readonlyLongArray expect:i-1];
            }
        }
    }
    
    if ( minElements < VERY_LONG_ARRAY_SIZE ) {
        // a few thousand elements in array
        NSArray *readonlyVeryLongArray=[NSArray arrayWithArray:self.veryLongArray];
        
        for(int i=0; i <= VERY_LONG_ARRAY_SIZE; i+=17) { 
            if ( runPasses || (runFails && i == 0) ) {
                [weakself addTestcaseToFind:i array:readonlyVeryLongArray expect:i-1];
            }
        }
        
        if ( runPasses ) {
            [weakself addTestcaseToFind:VERY_LONG_ARRAY_SIZE array:readonlyVeryLongArray expect:VERY_LONG_ARRAY_SIZE-1];
        }
        
        if ( runFails ) {
            [weakself addTestcaseToFind:0 array:readonlyVeryLongArray expect:-1];
            [weakself addTestcaseToFind:VERY_LONG_ARRAY_SIZE+2 array:readonlyVeryLongArray expect:-1];
        }
    }
    
    if ( self.debug ) NSLog(@"%s: buildAllTests contains %d elements", __PRETTY_FUNCTION__, (int) self.buildAllTests.count);
    
    self.allTests=[NSArray arrayWithArray:self.buildAllTests];
    [self.buildAllTests removeAllObjects];
    
    if ( self.debug ) NSLog(@"%s: allTests contains %d elements", __PRETTY_FUNCTION__, (int) self.allTests.count);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.viewController.runButton.enabled=YES;
    });
}

@end
