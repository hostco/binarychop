//
//  Constants.h
//  BinaryChop
//
//  Created by Howard Cohen on 10/7/16.
//  Copyright © 2016 Timefold.com. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

// do not modify these
#define LONG_ARRAY_SIZE 100   

typedef int (^ AlgorithmCompetionHandler) (int,  NSArray *, int);

static const NSString *TestingStringMessage=@"Testing now...";
static const NSString *kKeyAlgorithmNone=@"";
static const NSString *kKeyAlgorithm1=@"1";
static const NSString *kKeyAlgorithm2=@"2";
static const NSString *kKeyAlgorithm3=@"3";
static const NSString *kKeyAlgorithm4=@"4";
static const NSString *kKeyAlgorithm5=@"5";


// these are configurable
#define VERY_LONG_ARRAY_SIZE 4000
#define TOTAL_AVAILABLE_QUATLUUS    1000
#define REDUNDANT_CALCULATION_MULTIPLIER 100

// DEBUGGING.  Set to (YES) to enable a debugging trace
#define CHOP_OPERATION_DEBUG (NO)
#define QUATLUU_REQUEST_OPERATION_DEBUG (NO)
#define CHOP_WORKER_OPERATION_DEBUG  (NO)
#define CCMGR_DEBUG  (NO)

#endif /* Constants_h */
