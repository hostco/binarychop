//
//  QuatluuRequestOperation.h
//  BinaryChop
//
//  Created by Howard Cohen on 10/7/16.
//  Copyright © 2016 Timefold.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ChopOperation;
@class ConcurrencyManager;

@interface QuatluuRequestOperation : NSOperation

- (QuatluuRequestOperation *) initForAlgorithm:(NSString *) algkey_p forCCM:(ConcurrencyManager *) ccm_p;


@end
