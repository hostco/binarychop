//
//  QuatluuRequestOperation.m
//  BinaryChop
//
//  Created by Howard Cohen on 10/7/16.
//  Copyright © 2016 Timefold.com. All rights reserved.
//

#import "QuatluuRequestOperation.h"
#import "ConcurrencyManager.h"
#import "ChopOperation.h"
#import "Constants.h"
#import "AlgorithmMaker.h"
#import "ViewController.h"
#import "ChopWorker.h"

@interface QuatluuRequestOperation () {
    ConcurrencyManager * __weak ccm;
    NSString *algkey;
}

@property (nonatomic, weak) ConcurrencyManager *ccm;
@property (nonatomic, strong) NSString *algkey;
@end


@implementation QuatluuRequestOperation
@synthesize ccm, algkey;

- (QuatluuRequestOperation *) initForAlgorithm:(NSString *) algkey_p forCCM:(ConcurrencyManager *) ccm_p {
    if ( self = [super init] ) {
        self.algkey=algkey_p;
        self.ccm=ccm_p;
    }
    
    return self;
}

- (void)main {
    if ( ![self isCancelled] ) {
        // at this point we have access to take a quatluu
        
        BOOL debug=QUATLUU_REQUEST_OPERATION_DEBUG;
        if ( self.ccm.quatluus <= 0 ) {
            if ( debug ) NSLog(@"%s: FYI: no quatluus left ", __PRETTY_FUNCTION__);
        } else {
            self.ccm.quatluus--;
            
            if ( debug ) NSLog(@"%s: FYI: retrieved one quatluu, running an algorithm ", __PRETTY_FUNCTION__);
                NSOperationQueue *queue=self.ccm.workerQueues[self.algkey];
            if ( queue == nil ) {
                NSLog(@"%s: ERROR: cannot retrieve queue for algorithm %@", __PRETTY_FUNCTION__, self.algkey);
            } else {
                if (![self isCancelled]) { 
                    BOOL debug=QUATLUU_REQUEST_OPERATION_DEBUG;
                    if ( debug ) NSLog(@"%s: will run next test case for algorithm %@", __PRETTY_FUNCTION__, self.algkey);
                    
                    BOOL result=[self.ccm queueNextActionFor:algkey];  
                    if ( !result ) {
                        NSLog(@"FYI: chopWorker finished for algorithm %@", self.algkey);            
                    }
                }
            }
        }
    }
}


@end
