//
//  ViewController.h
//  BinaryChop
//
//  Created by Howard Cohen on 10/5/16.
//  Copyright © 2016 Timefold.com. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *algoResults1;
@property (weak, nonatomic) IBOutlet UILabel *algoResults2;
@property (weak, nonatomic) IBOutlet UILabel *algoResults3;
@property (weak, nonatomic) IBOutlet UILabel *algoResults4;
@property (weak, nonatomic) IBOutlet UILabel *algoResults5;
@property (weak, nonatomic) IBOutlet UIButton *runButton;


- (IBAction)runAction:(id)sender;
-(void) updateUIForAlgkey:(const NSString *) algkey results:(NSString *) results;

@end

