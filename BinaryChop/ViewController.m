//
//  ViewController.m
//  BinaryChop
//
//  Created by Howard Cohen on 10/5/16.
//  Copyright © 2016 Timefold.com. All rights reserved.
//

#import "ViewController.h"
#import "ConcurrencyManager.h"
#import "ChopOperation.h"
#import "QuatluuRequestOperation.h"
#import "ChopWorker.h"
#import "Constants.h"

@class ConcurrencyManager;

@interface ViewController () {
    ConcurrencyManager *ccm;
}

@property (nonatomic, strong)     ConcurrencyManager *ccm;


-(void) updateUIForAlgkey:(const NSString *) algkey results:(NSString *) results;
@end

@implementation ViewController
@synthesize ccm;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.ccm=[ConcurrencyManager getConcurrencyManager];
    self.ccm.viewController=self;
    
    self.algoResults1.text=(NSString *)TestingStringMessage;
    self.algoResults2.text=(NSString *)TestingStringMessage;
    self.algoResults3.text=(NSString *)TestingStringMessage;
    self.algoResults4.text=(NSString *)TestingStringMessage;
    self.algoResults5.text=(NSString *)TestingStringMessage;
}

/*
-(void) runAllAlgorithims:(int) findme array:(NSArray *)array expect:(int) expect {

    [self runAlgorithm:kKeyAlgorithm1 find:findme array:array expect:expect];
    
    [self runAlgorithm:kKeyAlgorithm2 find:findme array:array expect:expect];
    
    [self runAlgorithm:kKeyAlgorithm3 find:findme array:array expect:expect];
    
    [self runAlgorithm:kKeyAlgorithm4 find:findme array:array expect:expect];

    [self runAlgorithm:kKeyAlgorithm5 find:findme array:array expect:expect];
}
*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}


-(void) updateUIForAlgkey:(const NSString *) algkey results:(NSString *) results {

    if ( [algkey isEqualToString:(NSString *)kKeyAlgorithm1] ) {
        self.algoResults1.text=results;
    } else if ( [algkey isEqualToString:(NSString *)kKeyAlgorithm2] ) {
        self.algoResults2.text=results;
    } else if ( [algkey isEqualToString:(NSString *)kKeyAlgorithm3] ) {
        self.algoResults3.text=results;
    } else if ( [algkey isEqualToString:(NSString *)kKeyAlgorithm4] ) {
        self.algoResults4.text=results;
    } else if ( [algkey isEqualToString:(NSString *)kKeyAlgorithm5] ) {
        self.algoResults5.text=results;
    }
}

- (IBAction)runAction:(id)sender  {
    [self.ccm resetAllTests];   // reset for a new run
    
    for(NSString *algkey in @[kKeyAlgorithm1, kKeyAlgorithm2, kKeyAlgorithm3, kKeyAlgorithm4, kKeyAlgorithm5]) {
        NSOperationQueue *queue=self.ccm.workerQueues[algkey];
        if ( queue == nil ) {
            NSLog(@"%s: ERROR: could not fetch worker queue for algorithm %@", __PRETTY_FUNCTION__, algkey);
        } else {
            BOOL debug=CHOP_WORKER_OPERATION_DEBUG;
            if ( debug ) NSLog(@"%s: creating initial worker for algorithm %@", __PRETTY_FUNCTION__, algkey);

            ChopWorker *worker=[[ChopWorker alloc] initForAlgkey:algkey forCCM:self.ccm];
            [queue addOperation:worker];
        }
    }
}



@end
